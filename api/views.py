from django.shortcuts import render,HttpResponse
from django.db import connection
from django.conf import settings

from rest_framework.views import APIView
from rest_framework.generics import ListAPIView,UpdateAPIView,CreateAPIView
from rest_framework.status import HTTP_200_OK,HTTP_400_BAD_REQUEST
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response

from PIL import Image
import json
from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync
import asyncio


from registration.models import User
from .serializers import (UserGroupSerializers,
                          CreateUserProfileApi,
                          UserProfilePutSerializers
                         )
from realtime.models import pvChat



class UserGroups(ListAPIView):
    authentication_classes = []

    http_method_names = ['put','post','get']
    # serializer_class = UserGroupSerializers
    # queryset = pvChat.objects.raw("select id,pv_name,last_activity from private_chats where first_id = '4' or second_id = '4'")
    # queryset = pvChat.objects.raw("select creator_id)
    def findChatMembers(self,token,pvname=None):
        # print("this is token 2 ",token)
        result = None
        with connection.cursor() as cursor:
            cursor.execute("select id from c_users where token = '%s'"%(str(token)))
            userid = cursor.fetchone()
            if userid is not None:
                cursor.execute("select pv_name,first_id,second_id,creator_id,id from private_chats where first_id = '%d' or second_id = '%d'"%(int(userid[0]),int(userid[0])))
                result = cursor.fetchall()
                        
            info_list = []
            if result is not None:
                for i in result:
                    wanted = i[2] if i[2]!=userid[0] else i[1]
                    cursor.execute("select firstname,lastname,biography from c_users where id = '%d'"%(int(wanted)))
                    # cursor.execute("select firstname,lastname,biography from realtime_profile inner join c_users on c_users.id = realtime_profile.owner_id where c_users.id = '%d'"%int(wanted))
                    info_list.append(cursor.fetchone())

                for i in range(len(result)):
                    result[i]+=info_list[i]

                usernamelist = []
                for i in range(len(result)):
                    wanted = result[i][2] if result[i][2]!=userid[0] else result[i][1]
                    # print("this is the problem !:",result,wanted)
                    cursor.execute("select username,picture from c_users where id ='%d'"%(int(wanted)))
                    usernamelist.append(cursor.fetchone())

                for i in range(len(result)):
                    result[i]+=usernamelist[i]

                dic=[]
                for i in result:
                    currentdic = {}
                    currentdic.update({"channel_type":"pv",'name':i[5],'family':i[6],"username":i[8],"biography":i[7],"pv_name":i[0],"channel_id":i[4],"pic_url":i[9] if i[9] is not None else "/media/default"})
                    dic.append(currentdic)
                # return dic

                #finding user groups
                cursor.execute("select groupchat_id from group_chats_members where user_id = '{}' ".format(str(userid[0])))
                group_chats = cursor.fetchall()
                group_list =[]
                if group_chats is not None:
                    for i in group_chats:
                        cursor.execute("select * from group_chats where id = '{}'".format(int(i[0])))
                        group_list.append(cursor.fetchone())
                    
                    for i in group_list:
                        currentdic = {}
                        currentdic.update({"channel_type":"gp","group_name":str(i[2]),"group_description":str(i[5]),"group_num_of_members":int(i[3]),"channel_id":str(i[0])+"g","pic_url":"/media/groupdefault"}) 
                        dic.append(currentdic)
                    
                return dic


            else:
                return None
                
    def put(self,request,*args, **kwargs):
       return Response()

    def post(self,request,*args, **kwargs):
        token = request.data.get("token")
        data = self.findChatMembers(token)
        if data is not None:
            return Response({"data":data,"status":"api_get_chats_ok"})
        else:
            return Response({'data':[],"status":"api_get_chats_failed"})

class UserPvChat(ListAPIView):
    http_method_names =['put','post','get']
    authentication_classes = []

    def checkToken(self,email,token):
        with connection.cursor() as cursor:
            cursor.execute("select id from c_users where email = '%s' and token ='%s'"%(str(email),str(token)))
            result = cursor.fetchone()
            if result is not None:
                return True
            else:
                return False
    
    def serachUser(self,request,username):

        with connection.cursor() as cursor:
            cursor.execute("select email from c_users where username = '%s'"%(str(username)))
            foundUser = cursor.fetchone()
            if foundUser is not None:
                # cursor.execute("select last_login,firstname,lastname,biography from realtime_profile inner join c_users on c_users.id = realtime_profile.owner_id where c_users.username = '%s' "%(str(username)))
                cursor.execute("select last_login,firstname,lastname,biography,username,picture from c_users where username ='%s'"%(str(username)))
                user = cursor.fetchone()
                if user is not None:
                    return {"response":"succeful","data":user}
                # else:
                #     return {"response":"anonymosUser","data":None}
            else:
                return {"response":"targetUserDoesntExistsAnymore","data":None}

    def createPvChat(self,request,username,email):
        
        with connection.cursor() as cursor:
            cursor.execute("select email,id from c_users where username = '%s'"%(str(username)))
            found = cursor.fetchone()
            if found is not None:
                cursor.execute("select email,id from c_users where email = '%s'"%(str(email)))
                found2 = cursor.fetchone()
                if found2 is not None:
                    emails = [found[0],email]
                    emails.sort()
                    pvName = str(emails[0])+str(emails[1])
                    cursor.execute("insert into private_chats values (DEFAULT,DEFAULT,'%s','%d','%d','%d')"%(str(pvName),int(found2[1]),int(found2[1]),int(found[1])))
                    return True
                else:
                    return False
            else:
                return False

    def put(self,request):
        token = request.data.get('token')
        username = request.data.get('username')
        email = request.data.get('email_address')
        if self.checkToken(email,token):
            result = self.createPvChat(request,username,email)
            if result is not False:
                return Response({'status':"api_create_chat_ok"})
            else:
                return Response({'status':"api_create_chat_failed"})
        else:
            return Response({'status':"api_disconnect"})


    def post(self,request):
        token       = request.data.get('token')
        username    = request.data.get('username')
        result = self.serachUser(request,username)
        
        if result['response'] == "succeful":
            return Response({"status":"api_search_user_ok","firstname":str(result["data"][1]),"lastname":str(result["data"][2]),"biography":str(result["data"][3]),"last_activity":str(result["data"][0]),"username":str(result["data"][4]),"pic_url":str(result["data"][5] if result["data"][5] is not None else "/media/default")})

        elif result["response"]=="targetUserDoesntExistsAnymore":
            return Response({"status":"api_search_user_failed"})

class GroupChat(APIView):
    authentication_classes = []
    http_method_names = ['put','post']
    
    def checkToken(self,email,token):
        with connection.cursor() as cursor:
            
            cursor.execute("select id from c_users where email = '%s' and token ='%s'"%(str(email),str(token)))
            result = cursor.fetchone()
            if result is not None:
                return True,result[0]
            else:
                return False,None
    def put(self,request):
        email = request.data.get("email_address")
        token = request.data.get("token")
        gp_name = request.data.get("gp_name")
        added_username = json.loads(request.data.get("usernames"))
        description = request.data.get("description")
        # print(request.data)
        print("this is users :",added_username)
        # print(type())
        if self.checkToken(email,token)[0]:
            result = self.createGroup(email,gp_name,added_username,description)
            print(result)
            if result["status"] == "GroupCreatedAndUserAdded":

                layer = get_channel_layer()
                # await layer.send("",{})
                for i in result["cnames"]:
                    if i is not None:
                        async_to_sync(layer.group_add)(str(result["channel_id"]),str(i))
                        print ("send notification to ",i)
                for i in added_username:
                    async_to_sync(layer.group_send)(
                    str(i),
                    {
                        "type":"sendGroupInformationOnCreate",
                        "gp_name":str(gp_name),
                        "numofmember":str(len(added_username)),
                        "description":str(description),
                        'channel_id':str(result["channel_id"])
                    })
                return Response(data = {"status":"api_create_group_ok","channel_id":str(result["channel_id"])})
            elif result["status"] == "GroupDoesNotExists":
                return Response(data = {"status":"api_create_group_add_user_failed"})
            elif result["status"] == "CreatorDoesNotExists":
                return Response(data = {"status":"api_create_group_failed"})
            else:
                return Response(data = {"status":"api_create_group_failed"})
        else:
            return Response(data = {"status":"api_disconnect"})
        # return Response()
            
    def createGroup(self,email,gpname,members,description = None):
        with connection.cursor() as cursor:
            cursor.execute("select id from c_users where email = '{}'".format(str(email)))
            creator = cursor.fetchone()
            print(email)
            print(creator)
            
            if creator is not None:
                unique = str(email) + str(gpname)
                print(unique)
                cursor.execute("insert into group_chats values(DEFAULT,DEFAULT,'{}','{}',DEFAULT,'{}','{}','{}')".format(str(gpname),0,"group",str(unique),int(creator[0])))
                cursor.execute("select id from group_chats where uniqueness = '{}'".format(str(unique)))
                gp_id = cursor.fetchone()
                cnames = []
                if gp_id is not None:
                    for i in members:
                        cursor.execute("select id,cname from c_users where username = '{}'".format(str(i)))
                        userid = cursor.fetchone()
                        print("this is user id ",userid)
                        if userid is not None:
                            cnames.append(userid[1])
                            cursor.execute("insert into group_chats_members values(DEFAULT,'{}','{}')".format(int(gp_id[0]),int(userid[0])))
                            cursor.execute('''update group_chats set "numOfMember" = "numOfMember"+1 where uniqueness = '{}' '''.format(unique))
                        cursor.execute("select id from group_chats where uniqueness ='{}'".format(str(unique)))
                        channel_id = cursor.fetchone()
                    print("this is online channel names : ",cnames)
                    return {"status":"GroupCreatedAndUserAdded","channel_id":str(channel_id[0])+"g","cnames":cnames }
                else:
                    return {"status":"GroupDoesNotExists","channel_id":None}
            else:
                return {"status":"CreatorDoesNotExists","channel_id":None}

class groupMembers(APIView):
    http_method_names = ['post','put']
    authentication_classes = []
    def put(self,request):
        channel_id = request.data.get("channel_id")
        email = request.data.get("email_address")
        token = request.data.get("token")
        usernames = json.loads(request.data.get("usernames"))
        # print("data is ",request.data)
        if self.checkToken(email,token):
            if usernames is not None and usernames !=[]:
                self.addUserToGroup(channel_id,usernames)
                return Response(data = {"status":"api_group_user_add_ok"})
            else:
                return Response(data = {"status":"api_gorup_user_add_failed"})
        else:
            return Response(data = {"status":"api_disconnect"})

    def post(self,request):
        channel_id = request.data.get("channel_id")
        email = request.data.get("email_address")
        token = request.data.get("token")
        result = self.getMembers(channel_id)
        if self.checkToken(email,token):
            if result is not None:
                return Response(data = {"status":"api_get_group_members_ok","data":result})
            else:
                return Response(data={"status":"api_get_group_members_failed"})
        else:
            return Response(data = {"status":"api_disconnect"})


    def checkToken(self,email,token):
        with connection.cursor() as cursor:
            cursor.execute("select id from c_users where email = '%s' and token ='%s'"%(str(email),str(token)))
            result = cursor.fetchone()
            if result is not None:
                return True,result[0]
            else:
                return False,None

    
    def addUserToGroup(self,channel_id,usernames):
        with connection.cursor() as cursor:
            for i in usernames:
                # print("username in for is ",i)
                cursor.execute("select id from c_users where username = '{}'".format(str(i)))
                currentUser = cursor.fetchone()
                if currentUser is not None:
                    cursor.execute("insert into group_chats_members values(DEFAULT,'{}','{}')".format(int(channel_id[:-1]),int(currentUser[0])))
                    cursor.execute('''update group_chats set "numOfMember" = "numOfMember"+1 where id = '{}' '''.format(int(channel_id[:-1])))
                    # print("user added")


    def getMembers(self,channelId):
        with connection.cursor() as cursor:
            cursor.execute("select user_id from group_chats_members where groupchat_id = '{}' limit 20".format(int(channelId[:-1])))
            members_list = cursor.fetchall()
            
            if members_list is not None:
                user_info = []
                for i in members_list:
                    cursor.execute("select username,lastname,firstname,biography,picture from c_users where id = '{}'".format(int(i[0])))
                    result = cursor.fetchone()
                    if result is not None:
                        user_info.append(result)
                dic = []
                for i in user_info:
                    dic.append(({"username":str(i[0]),"name":str(i[2]),"family":str(i[1]),"biography":str(i[3]),"pic_url":str(i[4]) if i[4] is not None else "/media/default"}))
                
                return dic
            else:
                return None
                


class UserProfilePut(APIView):
    http_method_names = ['put','post','delete','get']
    authentication_classes = []
    parser_class = (MultiPartParser,FormParser)

    # queryset = profile.objects.raw("select * from realtime_profile")
    # serializer_class = CreateUserProfileApi
    def databaseconn(self,email,token,biography=None,firstname=None,lastname=None,username=None,picture = None):
        with connection.cursor() as cursor:
            
            profUrl = "/media/media/profiles/"+str(email)
            print(profUrl)
            with open(str(settings.BASE_DIR)+profUrl,"wb") as prof:
                cursor.execute("select email,token,id from c_users where email = '%s'"%(str(email)))
                data_back = cursor.fetchone()
                if data_back[0] == str(email) and data_back[1] == str(token):
                    # cursor.execute("insert into realtime_profile values(DEFAULT,DEFAULT,'%s','%s','%s',%d)"%(str(biography) if biography!=None else 'DEFAULT',str(firstname)if firstname!=None else 'DEFAULT',str(lastname) if lastname!=None else 'DEFAULT',data_back[2]))
                    cursor.execute("update c_users set username = '%s' ,picture = '%s' , biography = '%s' , firstname = '%s' ,lastname ='%s' where email = '%s'"%(str(username) if username!=None else 'DEFAULT',str(profUrl) if picture!='null' else 'DEFAULT',str(biography) if biography!=None else 'DEFAULT',str(firstname)if firstname!=None else 'DEFAULT',str(lastname) if lastname!=None else 'DEFAULT',str(email)))
                    if picture is not None:
                        prof.write(picture.read())
                    return True
                else:
                    return False
            
     


    def put(self,request):
        token = request.data.get('token')
        email = request.data.get('email_address')
        picture = request.data.get("picture")
        print("this is picture",picture=='null')

        if token and email:
            if self.databaseconn(email,token,request.data.get("biography"),request.data.get("firstname"),request.data.get("lastname"),request.data.get("username"),picture if picture!='null' else None):
                if picture=='null':
                    return Response(data = {"status":"api_update_profile_ok","pic_url":"/media/default/"})
                elif picture!= 'null':
                    return Response(data = {"status":"api_update_profile_ok","pic_url":"/media/media/profiles/"+str(email)})
            else:
                return Response(data = {"status":"api_update_profile_failed"})
        else:
            return Response({"status":"api_update_profile_not_enough_argument"})
            
    # def put(self,request):
    #     token = request.data.get('token')
    #     email = request.data.get('email_address')
    #     if token and email:
    #         # print(request.data.get("picture"))
    #         user = User.objects.get(email = str(email),token = str(token))
    #         user.picture = request.data.get("picture")
    #         user.biography = request.data.get("biography")
    #         user.firstname = request.data.get("firstname")
    #         user.lastname = request.data.get("lastname")
    #         print(user)
    #         serial = UserProfilePutSerializers(user)
    #         # if serial.is_valid():
    #         user.save()
    #             # serial.save()
    #         return Response(data = {"status":"api_update_profile_ok"})
    #         # else:
    #         #     print(serial.errors)
    #         #     return Response(data = {"status":"api_update_profile_failed"})
    #     else:
    #         return Response(data = {"status":"api_update_profile_not_enough_argument"})

    # def updateUser(self,request,email):
    #     instance = User.objects.get(email =str(email))
    #     instance


    def get(self,request):
        return Response(data = {"status":"b booogh"})
        
class SendMessagesByPvChat(APIView):
    http_method_names = ['put','post','delete','get']
    authentication_classes = []
    
    def getMessages(self,email,channelId,channelType):
        with connection.cursor() as cursor:

            if str(channelType) == "pv":
                cursor.execute('''select * from (select * from realtime_Pvmessagemodel where belongto_id = '{}' order by "messageId" desc limit 50) as foo order by "messageId" asc'''.format(int(channelId)))
                result = cursor.fetchall()
                
                print("tjos os resut",result)
                info_list =[]
                username_list = []
                if result is not None:
                    cursor.execute("select first_id,second_id from private_chats where id ='{}'".format(channelId))
                    idlist = cursor.fetchone()
                    
                    if idlist is not None:
                        for i in idlist:
                            cursor.execute("select username,picture from c_users where id = '{}'".format(str(i)))
                            # username_list.append({str(i):str(cursor.fetchone()[0])})
                            username_list.append(cursor.fetchone()[0])
                        for i in result:
                            wanted = str(username_list[0]) if str(idlist[0]) == str(i[5]) else username_list[1] 
                            info_list.append({"channel_type":"pv","message_text":str(i[0]),'message_type':str(i[1]),'message_id':str(i[2]),"send_date_time":str(i[3]),'sender_username':str(wanted)})
                        # print(info_list)
                        return info_list
                    else:
                        return None
                else:
                    return None

            elif str(channelType) == "gp":
                # print(channelId)
                cursor.execute('''select * from (select * from group_messages where belongto_id = '{}' order by "messageId" desc limit 50) as foo order by "messageId" asc'''.format(int(channelId[:-1])))
                result = cursor.fetchall()
                # print("this is result *****************************",result)
                if result is not None:
                    info_list =[]
                    userlist = []
                    for i in result:
                        userlist.append(i[5])
                    userlist = list(set(userlist))
                    user_info =[]
                    for i in userlist:
                        cursor.execute("select id,firstname,lastname,username,picture from c_users where id = '{}'".format(str(i)))
                        user_info.append(cursor.fetchone())
                    
                    for i in range(len(result)):
                        for j in user_info:
                            if int(j[0]) == int(result[i][5]):
                                result[i] = result[i]+j
                    # print("this is userlist",result[0])

                    dic = []
                    for i in result:
                        currentdic = {}
                        print("this is iiiiiii",i[10])
                        currentdic.update({"channel_type":"gp","message_text":str(i[0]),'message_type':str(i[1]),'message_id':str(i[2]),"send_date_time":str(i[3]),'sender_username':str(i[9]),"name":str(i[7]),"family":str(i[8]),"pic_url":str(i[10]) if i[10] is not None else "/media/default"}) 
                        # print(currentdic)
                        dic.append(currentdic)
                    print(dic)
                    return dic
                else:
                    return None


        
    def post(self,request):
        token = request.data.get('token')
        email = request.data.get('email_address')
        channel_id = request.data.get("channel_id")
        channel_type = request.data.get("channel_type")
        # print(request.data)
        if token and email and channel_id :
            databaseR = self.getMessages(email,channel_id,channel_type)
            print(databaseR)
            return Response(data={'status':"api_get_messages_ok",'data':databaseR},status=HTTP_200_OK)
        else:
            return Response(data={'status':'api_get_messages_failed','data':[]},status=HTTP_400_BAD_REQUEST)


