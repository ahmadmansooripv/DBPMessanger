from rest_framework.serializers import ModelSerializer
# from django.contrib.auth.models import User
from registration.models import User
from realtime.models import pvChat


class UserGroupSerializers(ModelSerializer):
    class Meta:
        model = pvChat
        fields = [
            'id',
            'pv_name',
            'last_activity',
        ]

class CreateUserProfileApi(ModelSerializer):

    # def create(self, validated_data):
    #     return super().create(validated_data)

    class Meta:
        model = User
        fields = [
            'owner',
            'biography',
            'firstname',
            'lastname',
        ]
class UserProfilePutSerializers(ModelSerializer):
    class Meta:
        model = User
        # fields =('picture','username','biography','firstname','lastname')
        fields = "__all__"