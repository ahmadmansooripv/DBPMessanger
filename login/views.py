from django.shortcuts import render
from django.views import View
from registration.passwordManager import checkPassword
from django.db import connection
from rest_framework.response import Response
from rest_framework.views import APIView

class UserLogin(APIView):
    authentication_classes = []
    def databaseConn(self,Email,password):
        with connection.cursor() as cursor:
            # print(Email)
            cursor.execute("select email,password,token,firstname,lastname,biography,username,picture from c_users where email = '{}'".format(str(Email)))
            data_back = cursor.fetchone()
            data = None
            # print(data_back)
            if data_back is not None:
                if data_back[0] == str(Email) and checkPassword(str(password),data_back[1]):
                    data = data_back[1]
                
                if not data:
                    status  = "api_login_failed"
                    return status,None
                            
                
                else:
                    status  = "api_login_ok"
                    data   = data_back
                    return status,data
            else:
                    status  = "api_login_failed"
                    return status,None

        
    def post(self,request):
        Email       = request.POST["email_address"]
        password    = request.POST["password"]
        # print(Email,password)
        if Email and password:
            database_respond = self.databaseConn(Email,password)
            if database_respond[0] == "api_login_ok":
                data = database_respond[1]
                # print("this is database",data)

                return Response(data = {"status":"api_login_ok",
                                        "auth_token":str(data[2]),
                                        "firstname":str(data[3]),
                                        "lastname":str(data[4]),
                                        "biography":str(data[5]),
                                        "username":str(data[6]),
                                        "pic_url":str(data[7]) if data[7] is not None else "/media/default"
                                        })
               
            # elif database_respond['login_status']=="api_login_failed":
            else:
                return Response(data = {
                    "status":"api_login_failed",
                    "auth_token":"Null"
                })
        else:
            return Response(data = {
                    "status":"api_login_failed",
                    "auth_token":"Null"

            })
                
