from asgiref.sync import async_to_sync
from channels.generic.websocket import AsyncWebsocketConsumer
from channels.consumer import AsyncConsumer
import json
import asyncio
from django.db import connection
from channels.auth import login,logout
from channels.db import database_sync_to_async  
from registration.models import User
import datetime
# from django.contrib.auth.models import User 


class ChatConsumer(AsyncWebsocketConsumer):
    
    def registerOnConnect(self,token):
        with connection.cursor() as cursor:
            # print('this is token',token)
            cursor.execute("SELECT id,email,username FROM c_users  WHERE token = '%s' "%(str(token)))
            result = cursor.fetchone()
            if result is not None:
            # print(User.objects.filter(email="ahmadmansuri3@gmail.com"))
            # res= User.objects.get(token=str(token))

                cursor.execute("update c_users set cname = '{}' where token = '{}'".format(str(self.channel_name),str(token)))
                
                login(self.scope,User.objects.filter(email=result[1]))
                self.scope["session"].save()
                # cursor.execute("update c_users set last_login = 'on' where token = '{}'".format(str(token)))
                return result[1],result[2]
            else:
                return None,None

    def onDisconnect(self,email):
        with connection.cursor() as cursor:
            print("try to set default value for channel name")
            cursor.execute("update c_users set cname = DEFAULT where email = '{}'".format(email))
            

    def createGroupList(self,email):
        with connection.cursor() as cursor:
            # inija mitoonim az user k bala vaght register save kardim toye scope estefade konim
            cursor.execute("select id from c_users where email ='%s' "%(str(email)))
            userId = cursor.fetchone()
            if userId is not None:
                cursor.execute("select pv_name,id from private_chats where first_id='%d' or second_id='%s'"%(int(userId[0]),int(userId[0])))
                result = cursor.fetchall()
                groupNameList = []
                if result is not None:
                    for i in result:
                        groupNameList.append(i[1])
                cursor.execute("select groupchat_id from group_chats_members where user_id ='{}'".format(int(userId[0])))
                grouplist = cursor.fetchall()
                if grouplist is not None:
                    for i in grouplist:
                        groupNameList.append(str(i[0])+"g")
            # print (groupNameList)
            return groupNameList

    def checkToken(self,email,token):
        with connection.cursor() as cursor:
            cursor.execute("select id from c_users where email = '%s' and token ='%s'"%(str(email),str(token)))
            result = cursor.fetchone()
            if result is not None:
                return True
            else:
                return False

    def serachUser(self,request,username):
        with connection.cursor() as cursor:
            cursor.execute("select email from c_users where username = '%s'"%(str(username)))
            foundUser = cursor.fetchone()
            if foundUser is not None:
                # cursor.execute("select last_login,firstname,lastname,biography from realtime_profile inner join c_users on c_users.id = realtime_profile.owner_id where c_users.username = '%s' "%(str(username)))
                cursor.execute("select last_login,firstname,lastname,biography from c_users where username = '%s'"%(str(username)))
                user = cursor.fetchone()
                if user is not None:
                    return {"response":"succeful","data":user}
                # else:
                #     return {"response":"anonymosUser","data":None}
            else:
                return {"response":"targetUserDoesntExistsAnymore","data":None} 

    def createPvChat(self,username,email):
        with connection.cursor() as cursor:
            cursor.execute("select email,id from c_users where username = '%s'"%(str(username)))
            found = cursor.fetchone()
            if found is not None:
                cursor.execute("select email,id,firstname,lastname,biography,username,picture from c_users where email = '%s'"%(str(email)))
                found2 = cursor.fetchone()
                if found2 is not None:
                    try:
                        # print('this is found2',found2)
                        emails = [found[0],email]
                        emails.sort()
                        pvName = str(emails[0])+str(emails[1])
                        cursor.execute("insert into private_chats values (DEFAULT,DEFAULT,'%s','%d','%d','%d')"%(str(pvName),int(found2[1]),int(found2[1]),int(found[1])))
                        return {'firstname':found2[2],'lastname':found2[3],'biography':found2[4],'username':found2[5],"pvname":pvName,'status':True,"pic_url":found2[6] if found2 is not None else "/media/default"}
                    except Exception as e:
                        print ("exception is :",e)
                        return {'status':'chatExists'}
                else:
                    return {'status':False}
            else:
                return {'status':False}

    def findUserIdWithUsername(self,username):
        with connection.cursor() as cursor:
            cursor.execute("select id from c_users where username ='{}'".format(str(username)))
            uid = cursor.fetchone()
            return uid[0]


    def findGroupIdByType(self,gpname,type = None):
        with connection.cursor() as cursor:
            cursor.execute("select id from private_chats where pv_name = '{}'".format(str(gpname)))
            userid =cursor.fetchone() 
            if userid is not None:
                return userid[0]
            else : 
                return None
        
    
    def saveMessageOnReceive(self,username,text,type,datetime,senderId,groupType,belongsTo):
        with connection.cursor() as cursor:
            if str(groupType) =="pv":
                userid = self.findUserIdWithUsername(username)
                print("THIS IS BELONGT TO :",belongsTo)
                # belongsTo = self.findGroupIdByType(gpname,groupType)
                cursor.execute("insert into realtime_Pvmessagemodel values ('{}','{}',DEFAULT,'{}','{}','{}')".format(str(text),str(type),str(datetime),int(belongsTo),int(userid)))
                return True,None,None
            elif str(groupType) =="gp":
                userid = self.findUserIdWithUsername(username)
                # belongsTo = self.findGroupIdByType(gpname,groupType)
                print(belongsTo,belongsTo[:-1])
                cursor.execute("insert into group_messages values ('{}','{}',DEFAULT,'{}','{}','{}')".format(str(text),str(type),str(datetime),int(belongsTo[:-1]),int(userid)))
                cursor.execute("select firstname,lastname,picture from c_users where username = '{}'".format(str(username)))
                result = cursor.fetchone()
                return True,str(result[0]),str(result[1]),str(result[2])
    
    
    def findUsernameByEmail(self,email,signal = None,token = None):
        with connection.cursor() as cursor:
            if signal is None:
                cursor.execute("select username from c_users where email = '{}'".format(str(email)))
                username = cursor.fetchone()
                if username is not None:
                    return str(username[0])
                else:
                    return None
            else:
                cursor.execute("select firstname,lastname,biography,username from c_users where email = '{}'".format(str(email)))
                info = cursor.fetchone()
                if info is not None:
                    return info
                else:
                    return None

    def findInfoByUsername(self,username,signal = None,token = None):
        with connection.cursor() as cursor:
            if signal is None:
                cursor.execute("select username from c_users where username = '{}'".format(str(username)))
                username = cursor.fetchone()
                if username is not None:
                    return str(username[0])
                else:
                    return None
            else:
                cursor.execute("select firstname,lastname,biography,picture from c_users where username = '{}'".format(str(username)))
                info = cursor.fetchone()
                if info is not None:
                    return info
                else:
                    return None
        
    def findCnameByUsername(self,username):
        with connection.cursor() as cursor:
            cursor.execute("select cname from c_users where username = '{}'".format(str(username)))
            result = cursor.fetchone()
            if result is not None:
                return result[0]
            else:
                return None

    async def connect(self):

        await self.accept()

    async def disconnect(self, code):
        self.onDisconnect(self.email)
        

    async def receive(self,text_data):
        r_data = json.loads(text_data)
        

        # ------- register section : register channel before anything else :) -------
        print("this is r_data -----------",r_data)

        if str(r_data["type"])=="register":
            print("this is channel_name : ",self.channel_name)
            result=  await database_sync_to_async(self.registerOnConnect)(str(r_data["token"]))
            if result[0] is not None and result[1] is not None:

                self.email = result[0]
                # self.username = await database_sync_to_async(self.findUsernameByEmail)(self.email)
                self.grouplist = await database_sync_to_async(self.createGroupList)(self.email)
                self.grouplist.append(str(result[1]))
                print('**************************** group list is ',self.grouplist)
                for group in self.grouplist:
                    await self.channel_layer.group_add(
                            str(group),
                            self.channel_name
                        )
            else:
                pass

        
        #--------- create new chat for channels --------------
        
        elif str(r_data["type"])=="realtime_create_chat":
            token = r_data['token']
            email = r_data['email_address']
            username = str(r_data['username'])
            print("this is username",username)
            if self.checkToken(email,token):
                result = await database_sync_to_async(self.createPvChat)(username,email)
                print("i am result",result)
                if result["status"] == True:
                    ucname = await database_sync_to_async(self.findCnameByUsername)(username)
                    channel_id = await database_sync_to_async(self.findGroupIdByType)(result['pvname'])
                    await self.channel_layer.group_add(
                        str(channel_id)
                        ,self.channel_name)

                    if ucname is not None:
                        await self.channel_layer.group_add(str(channel_id),str(ucname))
                    
                    print("this is channel id ",channel_id)
                    # await self.channel_layer.group_add(
                    #     str(self.channel_name),
                    #     str(self.channel_name)
                    #     )
                    # await self.channel_layer.group_add(
                    #     str(username),
                    #     str(username)
                    #     )

                    yourinfo = await database_sync_to_async(self.findInfoByUsername)(username,signal="alldata")
                    print(yourinfo)
                    print("this is yourinfo",yourinfo)
                    
                    await self.send(text_data = json.dumps({
                        'firstname':str(yourinfo[0]),
                        "lastname":str(yourinfo[1]),
                        "biography":str(yourinfo[2]),
                        "pic_url":str(yourinfo[3]) if yourinfo[3] is not None else "/media/default",
                        "username":str(username),
                        "pv_name":str(result["pvname"]),
                        "channel_id":str(channel_id),
                        "status":"realtime_create_chat_ok"
                    }))

                    await self.channel_layer.group_send(username,{
                        "type":"createNewChat",
                        'firstname':str(result['firstname']),
                        "lastname":str(result["lastname"]),
                        "biography":str(result["biography"]),
                        "username":str(result["username"]),
                        "pic_url":str(result['pic_url']),
                        "pvname":str(result["pvname"]),
                        "channel_id":str(channel_id),
                        "status":"realtime_create_chat_ok"
                    })
                
                elif result['status'] == 'chatExists':
                    self.send(text_data = json.dumps({
                        "type":"createNewChat",
                        "status":"realtime_create_chat_already_exists"
                    }))
                elif result['status'] == False:
                    self.send(text_data=json.dumps({
                        "type":"createNewChat",
                        "status":"realtime_create_chat_failed"
                    }))

            else:
                self.send(text_data = json.dumps({
                   "type":"realtime_disconnect" 
                }))

        # ----------------- send and save message -------------------


        elif str(r_data["type"]) == "realtime_send_message":
            print("this is data*** ",r_data)
            r_text = r_data["m_text"]
            r_datetime = str(datetime.datetime.now())
            r_sender = r_data['m_sender']
            # r_channel_type = r_data['m_channel_type']
            r_type = r_data['m_type']
            r_belongsTo = r_data['m_belongs_to']
            r_groupType = r_data['m_group_type']
            print("this is r_data",r_data)
            # r_groupname = r_data['m_gpname']
            result =  await database_sync_to_async(self.saveMessageOnReceive)(r_sender,r_text,r_type,r_datetime,r_sender,r_groupType,r_belongsTo)
            if result:
                if str(r_groupType) == "gp":
                    belong = str(r_belongsTo)
                    print(result)
                    await self.channel_layer.group_send(
                        belong,
                        {
                        "type":"sendResultWhereMessageSaved",
                        "message_text":str(r_text),
                        "send_date_time":str(r_datetime),
                        "sender_username":str(r_sender),
                        "message_type":str(r_type),
                        "message":"realtime_save_message_ok",
                        "sender_name":result[1],
                        "sender_family":result[2],
                        "pic_url":str(result[3]),
                        "channel_type":str(r_groupType),
                        "channel_id":str(belong)
                    })

                elif str(r_groupType) == "pv":
                    print(r_belongsTo)
                    belong = str(r_belongsTo)
                    await self.channel_layer.group_send(
                        belong,
                        {
                            "type":"sendResultWhereMessageSaved",
                            "message_text":str(r_text),
                            "send_date_time":str(r_datetime),
                            "sender_username":str(r_sender),
                            "message_type":str(r_type),
                            "message":"realtime_save_message_ok",
                            "channel_type":str(r_groupType)
                        } 
                    )
            else:
                await self.channel_layer.group_send(
                    str(r_belongsTo),
                    {
                        "type":"sendResultWhereMessageSaved",
                        "message":"realtime_save_message_failed"
                    } 
                )

    
        elif str(r_data["type"]) == "test":
            await self.channel_layer.group_send(
                str(r_data["channel_id"]),
                {
                    "type":"messageHandler",
                    "message":str(r_data["text_message"]),
                    "username":str(r_data['sender_username'])
            }
            ) 

        else:
            if self.email==None:
                await self.disconnect(None)

            else:
                await self.channel_layer.group_send(
                    # r_data['d_group'],
                    "mamad",
                    {
                        'type':"messageHandler",
                        'message':str(r_data['text_message']),
                        'username':str(r_data['sender_username'])
                    }
                )


    async def messageHandler(self,event):
        message =event['message']
        senderUsername = event['username']

        await self.send(text_data=json.dumps(
            {
                'text_message':str(message),
                'sender_username':str(senderUsername)
            }
        ))

    async def sendResultWhereMessageSaved(self,event):
        if str(event['channel_type']) == "pv": 
            await self.send(text_data=json.dumps({
                'type':"message_transition_checked",
                'message':event['message'],
                "text_message":event['message_text'],
                "send_date_time":event['send_date_time'],
                "sender_username":event['sender_username'],
                "message_type":event['message_type'],
                "channel_type":event['channel_type'],
                
                
                

            }))
        elif str(event['channel_type']) == "gp":
            await self.send(text_data=json.dumps({
                'type':"message_transition_checked",
                'message':event['message'],
                "text_message":event['message_text'],
                "send_date_time":event['send_date_time'],
                "sender_username":event['sender_username'],
                "sender_name":event["sender_name"],
                "sender_family":event["sender_family"],
                "message_type":event['message_type'],
                "channel_type":event['channel_type'],
                "channel_id":event['channel_id'],
                "pic_url":event['pic_url']
            }))

    async def createNewChat(self,event):
        await self.send(text_data=json.dumps(
            {
                'type':'realtime_create_chat_ok',
                'username':str(event['username']),
                'firstname':str(event['firstname']),
                'lastname':str(event['lastname']),
                'biography':str(event['biography']),
                'pv_name' :str(event['pvname']),
                'channel_id':str(event['channel_id']),
                "pic_url":event['pic_url']
            }
        ))

    async def sendGroupInformationOnCreate(self,event):
        await self.send(text_data=json.dumps({
            "type":"realtime_create_group_ok",
            "gp_name":str(event['gp_name']),
            "description":str(event['description']),
            "numofmember":str(event['numofmember']),
            "channel_id":str(event["channel_id"])
        }))